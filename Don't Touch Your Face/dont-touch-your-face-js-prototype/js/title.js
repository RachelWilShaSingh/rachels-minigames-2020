console.log( "title.js" );

titleState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,

    Init: function( canvas, options ) {
        titleState.canvas = canvas;
        titleState.options = options;
        titleState.isDone = false;

        /*
        ASSET_TOOLS.AddSound( { title: "title",
                            file: "assets/audio/DancingBunnies_Moosader.ogg",
                            volume: 0.1,
                            loop: true,
                            isMusic: true
                            } );
        */

        UI_TOOLS.CreateImage( { title: "background", src: "assets/ui/title.png",
                            x: 0, y: 0, width: 720, height: 720, fullWidth: titleState.options.width, fullHeight: titleState.options.height } );
                            
        UI_TOOLS.CreateText( { title: "Title", words: LANGUAGE_TOOLS.GetText( "English", "title" ),
                            color: "#000000", font: "bold 53px Sans-serif", x: 20, y: 175 } );

        UI_TOOLS.CreateText( { title: "by", words: LANGUAGE_TOOLS.GetText( "English", "author" ), x: 200, y: 200,
                             color: "#000000", font: "bold 20px Sans-serif",
                              } );

        UI_TOOLS.CreateButton( { title: "playButton", words: LANGUAGE_TOOLS.GetText( "English", "play" ),
                             color: "#ffffff", font: "bold 30px Sans-serif",
                             src: "assets/ui/button.png",
                             x: 0, y: options.height - 150, textX: 85, textY: 35, width: 250, height: 100, fullWidth: 250, fullHeight: 50,
                             Click: function() {
                                 main.changeState( "gameState" );
                                 } } );

        UI_TOOLS.CreateButton( { title: "helpButton", words: LANGUAGE_TOOLS.GetText( "English", "help" ),
                             color: "#ffffff", font: "bold 30px Sans-serif",
                             src: "assets/ui/button.png",
                             x: 240, y: options.height - 150, textX: 85, textY: 35, width: 250, height: 100, fullWidth: 250, fullHeight: 50,
                             Click: function() {
                                 main.changeState( "helpState" );
                                 } } );

        UI_TOOLS.CreateButton( { title: "optionsButton", words: LANGUAGE_TOOLS.GetText( "English", "options" ),
                             color: "#ffffff", font: "bold 30px Sans-serif",
                             src: "assets/ui/button.png",
                             x: 470, y: options.height - 150, textX: 60, textY: 35, width: 250, height: 100, fullWidth: 250, fullHeight: 50,
                             Click: function() {
                                 main.changeState( "optionsState" );
                                 } } );
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    KeyPress: function( ev ) {
    },

    KeyRelease: function( ev ) {
    },

    Update: function() {
    },

    Draw: function() {
        UI_TOOLS.Draw( titleState.canvas );
    },
};
