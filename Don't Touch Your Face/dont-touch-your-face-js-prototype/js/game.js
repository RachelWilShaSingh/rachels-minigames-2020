console.log( "game.js" );

playerHand = {
    x: 0,
    y: 0,
    speed: 2,
    idleSpeed: 1,
    hotspotX: 177/2,
    hotspotY: 120,
    frameX: 0,
    frameY: 0,
    frameWidth: 177,
    frameHeight: 920,
    imageWidth: 708,
    imageHeight: 920,
    image: null,
    
    GetHotspotXY: function() {
        var hotspot = {
            x: playerHand.hotspotX + playerHand.x,
            y: playerHand.hotspotY + playerHand.y,
        };
        return hotspot;
    },
    
    Update( level, faceX, faceY ) {
        if ( level == 1 )
        {
            // Gravitate towards face
            if      ( faceX < playerHand.x ) {   playerHand.x -= playerHand.idleSpeed; }
            else if ( faceX > playerHand.x ) {   playerHand.x += playerHand.idleSpeed; }
            if      ( faceY < playerHand.y ) {   playerHand.y -= playerHand.idleSpeed; }
            else if ( faceY > playerHand.y ) {   playerHand.y += playerHand.idleSpeed; }
        }
        else if ( level == 2 )
        {
        }
    },

    Draw: function( canvas ) {
        canvas.drawImage(
            playerHand.image,
            playerHand.frameX,
            playerHand.frameY,
            playerHand.frameWidth,
            playerHand.frameHeight,
            playerHand.x,
            playerHand.y,
            playerHand.frameWidth,
            playerHand.frameHeight,
        );
        
        // Debug: Draw hotspot
        canvas.beginPath();
        canvas.rect( 
            playerHand.hotspotX + playerHand.x, 
            playerHand.hotspotY + playerHand.y,
            10, 
            10 );
        canvas.stroke();
    },
    
    Move: function( mouseX, mouseY ) {
        playerHand.x = mouseX - playerHand.frameWidth / 2;
        playerHand.y = mouseY - playerHand.frameHeight * 0.15;
    }
};

faceObj = {
    x: 720/2 - 301/2,
    y: 720/2 - 186/2,
    hotspotX: 150,
    hotspotY: 90,
    speed: 2,
    frameX: 0,
    frameY: 0,
    frameWidth: 301,
    frameHeight: 186,
    imageWidth: 301,
    imageHeight: 186,
    image: null,
    
    GetHotspotXY: function() {
        var hotspot = {
            x: faceObj.hotspotX + faceObj.x,
            y: faceObj.hotspotY + faceObj.y,
        };
        return hotspot;
    },

    Draw: function( canvas ) {
        canvas.drawImage(
            faceObj.image,
            faceObj.frameX,
            faceObj.frameY,
            faceObj.frameWidth,
            faceObj.frameHeight,
            faceObj.x,
            faceObj.y,
            faceObj.frameWidth,
            faceObj.frameHeight,
        );
        
        // Debug: Draw hotspot
        canvas.beginPath();
        canvas.rect( 
            faceObj.hotspotX + faceObj.x, 
            faceObj.hotspotY + faceObj.y,
            10, 
            10 );
        canvas.stroke();
    },
    
    Move: function( level, mouseX, mouseY ) {
        if ( level == 2 || level == 3 )
        {
            // React to the mouse
            if ( mouseX != null && mouseY != null )
            {
                if ( mouseX < faceObj.x )
                {
                    faceObj.x -= faceObj.speed;
                }
                else if ( mouseX > faceObj.x )
                {
                    faceObj.x += faceObj.speed;
                }
                
                if ( mouseY < faceObj.y )
                {
                    faceObj.y -= faceObj.speed;
                }
                else if ( mouseY > faceObj.y )
                {
                    faceObj.y += faceObj.speed;
                }
            }
        }
    }
};

function CloneObject( obj )
{
    var clone = $.extend( true, {}, obj );
    return clone;
}

gameState = {
    canvas: null,
    options: {},
    images: {},
    isDone: false,
    playerHand : null,
    faces: [],
    level: 1,
    levelTimer: 0,
    startLevelCountdown: 0,
    extraLives: 1,
    paused: false,
    
    keys: {
        P:         { code: "p", isDown: false }, 
    },


    Init: function( canvas, options ) {
        gameState.canvas = canvas;
        gameState.options = options;
        gameState.isDone = false;

        gameState.playerHand = playerHand;

        gameState.images.players = new Image();
        gameState.images.players.src = "assets/images/player.png";
        gameState.playerHand.image = gameState.images.players;

        gameState.images.faces = new Image();
        gameState.images.faces.src = "assets/images/faces.png";
        
        var face = faceObj;
        face.image = gameState.images.faces;
        gameState.faces.push( face );
        
        gameState.SetupLevel( 1 );
    },

    SetupLevel: function( level ) {
        gameState.level = level;
        
        gameState.playerHand.x = 720/2 - 50;
        gameState.playerHand.y = 720-100;
        gameState.startLevelCountdown = 50;
            gameState.levelTimer = 500;
        
        if ( level == 1 ) {
        }
        else if ( level == 2 ) {
        }
        else if ( level == 3 ) {            
            var faceClone = CloneObject( gameState.faces[0] );
            faceClone.x = 0;
            faceClone.y = 0;
            gameState.faces.push( faceClone );
            
            console.log( gameState.faces );
        }
        
        UI_TOOLS.CreateText( { 
            title: "level-text", 
            words: "Level " + gameState.level.toString(), 
            color: "#000000", 
            font: "bold 40px Sans-serif", 
            x: 720/2 - 75, 
            y: 720/2
        } );
    },
    
    Distance: function( objA, objB ) {        
        var xdiff = objA.GetHotspotXY().x - objB.GetHotspotXY().x;
        var ydiff = objA.GetHotspotXY().y - objB.GetHotspotXY().y;
        
        var xdiff2 = xdiff * xdiff;
        var ydiff2 = ydiff * ydiff;
        
        var distance = Math.sqrt( xdiff2 + ydiff2 );
        
        //console.log( "Distance?", objA.GetHotspotXY(), objB.GetHotspotXY(), distance );
        
        return distance;
    },
    
    DebugSetup() {
        UI_TOOLS.CreateText( { 
            title: "player-position", 
            words: "Player Position: " 
                + gameState.playerHand.x.toString() + ", " 
                + gameState.playerHand.y.toString(), 
            color: "#000000", 
            font: "bold 10px Sans-serif", 
            x: 720 - 200, 
            y: 10
        } );  
        UI_TOOLS
        .CreateText( { 
            title: "player-hotspot", 
            words: "Player Hotspot: " 
                + gameState.playerHand.GetHotspotXY().x.toString() + ", " 
                + gameState.playerHand.GetHotspotXY().y.toString(), 
            color: "#000000", 
            font: "bold 10px Sans-serif", 
            x: 720 - 200, 
            y: 20
        } );
        
        for ( var i = 0; i < gameState.faces.length; i++ )
        {
            UI_TOOLS.CreateText( { 
                title: "face-position", 
                words: "Face Position: " 
                    + gameState.faces[i].x.toString() + ", " 
                    + gameState.faces[i].y.toString(), 
                color: "#000000", 
                font: "bold 10px Sans-serif", 
                x: 720 - 200, 
                y: 50
            } );
            
            UI_TOOLS.CreateText( { 
                title: "face-hotspot", 
                words: "Face Hotspot: " 
                    + gameState.faces[i].GetHotspotXY().x.toString() + ", " 
                    + gameState.faces[i].GetHotspotXY().y.toString(), 
                color: "#000000", 
                font: "bold 10px Sans-serif", 
                x: 720 - 200, 
                y: 60
            } );
            
            UI_TOOLS.CreateText( { 
                title: "distance", 
                words: "Distance: " + gameState.Distance( gameState.playerHand, gameState.faces[i] ),
                color: "#000000", 
                font: "bold 10px Sans-serif", 
                x: 720 - 200, 
                y: 90
            } );  
        }  
        
        UI_TOOLS.CreateText( {
            title: "face-count", 
            words: "Blob",
            color: "#000000", 
            font: "bold 10px Sans-serif", 
            x: 720 - 200, 
            y: 110
        } );
        
    },
    
    DebugUpdate() {
        UI_TOOLS.UpdateText( "player-position", "Player Position: " + gameState.playerHand.x.toString() + ", " + gameState.playerHand.y.toString() );
        UI_TOOLS.UpdateText( "player-hotspot", "Player Hotspot: " + gameState.playerHand.GetHotspotXY().x.toString() + ", " + gameState.playerHand.GetHotspotXY().y.toString() );
        
        UI_TOOLS.UpdateText( "face-count", "Total Faces: " + gameState.faces.length.toString() );
        
        for ( var i = 0; i < gameState.faces.length; i++ )
        {
            UI_TOOLS.UpdateText( "face-position", "Face Position: " + gameState.faces[i].x.toString() + ", " + gameState.faces[i].y.toString() );
            UI_TOOLS.UpdateText( "face-hotspot", "Face Hotspot: " + gameState.faces[i].GetHotspotXY().x.toString() + ", " + gameState.faces[i].GetHotspotXY().y.toString() );
            UI_TOOLS.UpdateText( "distance", "Distance: " + gameState.Distance( gameState.playerHand, gameState.faces[i] ) );
        }
    },

    Clear: function() {
        UI_TOOLS.ClearUI();
    },

    Click: function( ev ) {
        UI_TOOLS.Click( ev );
    },

    KeyPress: function( ev ) {
        if ( ev.key == "Escape" ) {
            main.changeState( "titleState" );
        }

        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = true;
            }
        } );
    },

    KeyRelease: function( ev ) {
        $.each( gameState.keys, function( i, key ) {
            if ( ev.key == key.code ) {
                key.isDown = false;
            }
        } );
    },

    MouseMove: function( ev ) {
        if ( gameState.startLevelCountdown <= 0 )
        {
            var canvasRect = gameState.canvas.canvas.getBoundingClientRect();
            var adjustedX = ev.clientX - canvasRect.left;
            var adjustedY = ev.clientY - canvasRect.top;
            
            gameState.playerHand.Move( adjustedX, adjustedY );
            
            for ( var i = 0; i < gameState.faces.length; i++ )
            {
                gameState.faces[i].Move( gameState.level, adjustedX, adjustedY );
            }
        }
    },

    SetupHud() {
        UI_TOOLS.CreateText( { 
            title: "player-lives", 
            words: "Extra Lives: " 
                + gameState.extraLives, 
            color: "#000000", 
            font: "bold 20px Sans-serif", 
            x: 10, 
            y: 20
        } );
        UI_TOOLS.CreateText( { 
            title: "timer", 
            words: "Time to survive: " 
                + gameState.levelTimer, 
            color: "#000000", 
            font: "bold 20px Sans-serif", 
            x: 10, 
            y: 40
        } );
    },
    
    UpdateHud() {
        UI_TOOLS.UpdateText( "timer", "Time to survive: " + gameState.levelTimer );
    },

    Update: function() {  
        if ( gameState.keys.P.isDown ) {
            gameState.paused = !gameState.paused;
        }
      
        if ( gameState.startLevelCountdown > 0 ) 
        {
            // Paused to show current level
            
            gameState.startLevelCountdown--;
            if ( gameState.startLevelCountdown == 0 )
            {
                UI_TOOLS.ClearUI();                
                gameState.DebugSetup();
                gameState.SetupHud();
            }
        }
        else if ( !gameState.paused )
        {
            gameState.DebugUpdate();
            gameState.UpdateHud();
            
            // Game playing
            for ( var i = 0; i < gameState.faces.length; i++ )
            {
                gameState.faces[i].Move( gameState.level, null, null );
                
                // Distance between hand and face
                var distance = gameState.Distance( gameState.playerHand, gameState.faces[i] );
                //console.log( distance );
                
                if ( distance < 100 )
                {
                    gameState.LoseLife();
                }
            }
            
            if ( gameState.level == 1 )
            {
                gameState.playerHand.Update( gameState.level, gameState.faces[0].x, gameState.faces[0].y );
            }
            else
            {
            }
            
            // Level timer
            gameState.levelTimer--;
            if ( gameState.levelTimer == 0 )
            {
                // Finished level!!
                gameState.SetupLevel( gameState.level + 1 );
            }
        }
    },
    
    LoseLife() {
        gameState.extraLives--;
        
        // Game over
        if ( gameState.extraLives < 0 )
        {
            gameState.GameOver();
        }
        else
        {
            gameState.SetupLevel( gameState.level );
        }
    },

    GameOver() {
        UI_TOOLS.ClearUI(); 
        UI_TOOLS.CreateText( { 
            title: "game-over", 
            words: "Game Over", 
            color: "#ffffff", 
            font: "bold 40px Sans-serif", 
            x: 720/2 - 100, 
            y: 720/2
        } );  
        
    },

    Draw: function() {
        if ( gameState.extraLives >= 0 )
        {
            // Draw grass 9cc978
            main.canvasWindow.fillStyle = "#9cc978";
            main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );
            
            // Draw faces
            for ( var i = 0; i < gameState.faces.length; i++ )
            {
                gameState.faces[i].Draw( main.canvasWindow );
            }

            // Draw player
            gameState.playerHand.Draw( main.canvasWindow );
        }
        else
        {
            // Game over state
            main.canvasWindow.fillStyle = "#000000";
            main.canvasWindow.fillRect( 0, 0, main.settings.width, main.settings.height );            
        }

        UI_TOOLS.Draw( gameState.canvas );
    },

    ClickPlay: function() {
    }
};
