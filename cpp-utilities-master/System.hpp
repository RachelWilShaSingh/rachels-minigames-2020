#ifndef SYSTEM_HPP
#define SYSTEM_HPP

#include <string>
using namespace std;

class System
{
    public:
    static void CreateDirectory( const string& directory, bool relative = true );

    private:
    static void CreateRelativeDirectory( const string& directory );
};

void System::CreateDirectory( const string& directory, bool relative )
{
    if ( relative )
    {
        CreateRelativeDirectory( directory );
    }
}

void System::CreateRelativeDirectory( const string& directory )
{
    #if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
        string cmd = "mkdir " + directory;
        system( cmd.c_str() );
    #else
        string cmd = "mkdir " + directory;
        system( cmd.c_str() );
    #endif
}

#endif
