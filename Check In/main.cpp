#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
using namespace std;

#include "Functionality.hpp"
#include "../cpp-utilities-master/Menu.hpp"
#include "../cpp-utilities-master/StringUtil.hpp"

int main()
{
    srand( time( NULL ) );

    // Set up
    vector<string> listOfNames = LoadFriends();
    vector<Friend> friends;
    vector<string> friendNames;

    GetFriends( listOfNames, friends, friendNames );
    Friend you;
    you.name = "You";

    int day = 1;

    // Intro
    Menu::ClearScreen();
    Menu::Header( "Check In, a mini game by Rachel Singh, April 2020" );
    cout    << "We are all social-distancing in our homes, only going outside" << endl
            << "for bare necessities. Luckily, we can still keep in touch via" << endl
            << "the internet - and it's important to keep in touch." << endl << endl
            << "Friends will reach out to you, and each day you can" << endl
            << "choose which friend to reach out to. We're in this together" << endl
            << "and being isolated is just going to make us less happy and less healthy." << endl << endl
            << "Press a button to start the game." << endl;

    Menu::Pause();

    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "Day " + StringUtil::ToString( day ) );

        cout << "Stats:" << endl;
        you.DisplayStats();

        cout << "Who should you reach out to today?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( friendNames ) - 1;

        cout << "You reached out to " << friends[ choice ].name << "!" << endl;

        bool someoneReachedOut = false;
        for ( auto & fr : friends )
        {
            bool choosesToReachOut = fr.ChooseToReachOut();

            if ( choosesToReachOut )
            {
                someoneReachedOut = true;
                cout << fr.name << " reached out to you today!" << endl;
                you.AdjustHappiness( 4 );
            }
        }

        if ( !someoneReachedOut )
        {
            cout << "Nobody reached out to you today." << endl;
            you.AdjustHappiness( -4 );
        }

        day++;

        Menu::Pause();
    }

    return 0;
}
