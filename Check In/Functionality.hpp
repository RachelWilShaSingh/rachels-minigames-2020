#ifndef _FUNCTIONALITY_HPP
#define _FUNCTIONALITY_HPP

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>
using namespace std;

class Friend
{
    public:
    Friend()
    {
        happiness = 100;
    }

    string name;
    int happiness;

    bool ChooseToReachOut()
    {
        return ( rand() % 50 == 0 );
    }

    void DisplayStats()
    {
        if ( name == "You" )
        {
            cout << name << " are feeling " << happiness << "% happy." << endl;
        }
        else
        {
            cout << name << " is feeling " << happiness << "% happy." << endl;
        }
    }

    void AdjustHappiness( int amount )
    {
        happiness += amount;

        if ( happiness < 0 ) { happiness = 0; }
        if ( happiness > 100 ) { happiness = 100; }
    }
};

vector<string> LoadFriends()
{
    vector<string> listOfNames;

    ifstream input( "namelist.txt" );
    string buffer;

    input >> buffer; // ignore header
    while ( input >> buffer )
    {
        listOfNames.push_back( buffer );
    }

    return listOfNames;
}

void GetFriends( vector<string>& listOfNames, vector<Friend>& friends, vector<string>& friendNames )
{
    vector<int> usedIndices;

    for ( int i = 0; i < 8; i++ )
    {
        // Get a unique name
        int randomIndex = rand() % listOfNames.size();
        while ( find( usedIndices.begin(), usedIndices.end(), randomIndex ) != usedIndices.end() )
        {
            randomIndex = rand() % listOfNames.size();
        }

        cout << "Index " << randomIndex << endl;

        Friend newFriend;
        newFriend.name = listOfNames[ randomIndex ];
        friends.push_back( newFriend );
        friendNames.push_back( listOfNames[ randomIndex ] );
    }
}

#endif
