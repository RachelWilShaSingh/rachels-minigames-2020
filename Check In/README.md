# Check In

```
--------------------------------------------------------------------------------
 | Check In, a mini game by Rachel Singh, April 2020 |
 -----------------------------------------------------

We are all social-distancing in our homes, only going outside
for bare necessities. Luckily, we can still keep in touch via
the internet - and it's important to keep in touch.

Friends will reach out to you, and each day you can
choose which friend to reach out to. We're in this together
and being isolated is just going to make us less happy and less healthy.

Press a button to start the game.

 Press ENTER to continue...
```

![Screenshot](screenshot.png)

## Game Hints

**Help! Nobody checks in on me!**

Yep, I know.
If you want to modify the game, line 26 in Functionality.hpp has

```
bool ChooseToReachOut()
{
    return ( rand() % 50 == 0 );
}
```

you can change 50 to a different value so the chances of someone
reaching out to you is better than 1 in 50.
